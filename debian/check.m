figure (1, "visible", "off");

### Override the core function usejava, in order to prevent
### graphical dialogs using the AWT toolkit
function retval = usejava (x)
  retval = 0;
endfunction

global demos
global count

function retval = input ()
  global demos
  global count
  retval = num2str (demos (count++));
endfunction

## The 7th test has a submenu, with 7 elements (hence the 1 : 7).
## FIXME: the 4th test is currently disabled, because it enters an infinite loop
## (tested with tsa 4.4.5 and octave 5.1.0).
demos = [1 : 3, 5 : 7, 1 : 7, 8 : 15];
count = 1;
tsademo;

invfdemo;

bisdemo;

